export type Address = {
  building: string;
  house_number: string;
  road: string;
  neighbourhood: string;
  suburb: string;
  city_district: string;
  city: string;
  postcode: string;
  country: string;
  country_code: string;
}

export type LocationData = {
  place_id: number;
  licence: string;
  osm_type: string;
  osm_id: number;
  boundingbox: string[];
  lat: string;
  lon: string;
  display_name: string;
  class: string;
  type: string;
  importance: number;
  address: Address;
  svg: string;
}