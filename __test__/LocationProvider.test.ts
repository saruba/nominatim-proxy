import LocationProvider from '../src/LocationProvider'

describe('LocationProvider', () => {
  it('provides location data for a specified query', async () => {
    const provider = new LocationProvider()
    const query = 'cullera'
    const expectedCity = 'Cullera'

    const locations = await provider.search(query)
    const firstCity = locations[0]

    expect(firstCity.address.city).toBe(expectedCity)

  })
})
