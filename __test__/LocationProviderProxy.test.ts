import LocationProviderProxy from '../src/LocationProviderProxy'

describe('LocationProviderProxy', () => {

  it('provides location data for a specified query', async () => {

    const provider = new LocationProviderProxy()
    const query = 'cullera'
    const expectedCity = 'Cullera'

    const locations = await provider.search(query)
    const firstCity = locations[0]

    expect(firstCity.address.city).toBe(expectedCity)

  })
})
